def translate(sentence)
  words = sentence.split
  pigwords = words.map do |word|
    latinize(word)
  end
  pigwords.join(" ")
end

def latinize(word)
  vowels="aeiou"
  if vowels.include?(word[0])
    word=word+"ay"
  else
    vowelcounter = 1
    until vowels.include?(word[vowelcounter]) && (word[vowelcounter-1..vowelcounter] != "qu")
      vowelcounter += 1
    end
    word=word[vowelcounter..-1]+word[0...vowelcounter]+"ay"
  end
end
