def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str,n=2)
  str_new=str<<" "
  str_new=str_new*(n)
  str_new.strip
end

def start_of_word(str,n)
  str[0...n]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  words = str.split
  small_words = "an and a the of in over"
  words.each_with_index do |word,idx|
    if small_words.include?(word)==false
      word.capitalize!
    end
  end
  words[0].capitalize!
  words.join(" ")
end
